# parquet-archiver

This is the basic PoC code for reading in many S3 directories with JSON logging files within them, combining into one
Spark DataFrame and saving to Parquet.

Not been tested fully in some time so a pseudo workflow for using this would be:

* create a new S3 bucket or folder with all your data prepared within it
* use sbt or similar to build the JAR
* create a Spark cluster (preference is AWS EMR)
* via spark submit sent this job to the cluster
* wait / monitor and then parquet file(s) should appear in S3

The phishing-etl repo is already in a good shape with respect to running so you should be able to use that as a model 
here.
