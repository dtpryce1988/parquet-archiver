package com.wandera.datascience.spark.context

import com.wandera.datascience.spark.context.LocalSparkContext.log
import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.{SparkConf, SparkContext}

trait LocalSparkContext {

  Logger.getLogger("akka").setLevel(Level.ERROR)
  Logger.getLogger("com.datastax.driver").setLevel(Level.ERROR)
  Logger.getLogger("com.datastax.spark").setLevel(Level.ERROR)
  Logger.getLogger("org.apache.hadoop").setLevel(Level.ERROR)
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)
  Logger.getLogger("org.spark_project").setLevel(Level.ERROR)

  val sparkConf: SparkConf = new SparkConf()

  def getSparkContext = {
    sparkConf.setAppName(getClass.getName).setMaster("local[*]")
      .set("spark.driver.maxResultSize", "15g")
      .set("spark.driver.bindAddress","127.0.0.1")
    log.info("About to initialise Local SparkContext.")
    new SparkContext(sparkConf)
  }

}

object LocalSparkContext {
  @transient lazy val log = LogManager.getLogger(getClass.getName)
}
