package com.wandera.datascience.spark.export

import com.wandera.datascience.spark.sink.S3Sink
import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark._
import org.apache.spark.sql.SparkSession
import com.amazonaws.services.s3._
import model._
import com.amazonaws.auth.BasicAWSCredentials

import scala.collection.JavaConverters._
import scala.io.Source
import java.io.InputStream
import java.io.BufferedInputStream

import org.apache.spark.sql._
import org.zalando.spark.jsonschema.SchemaConverter
import java.util.zip.GZIPInputStream

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration

object ParquetArchiver
  extends App
  with S3Sink {
  val log = LogManager.getLogger(getClass.getName)

  def listToOptionMap(argList: List[String]): Map[String, Any] = {
    def nextOption(optionMap: Map[String, Any], argList: List[String]): Map[String, Any] = {
      argList match {
        case Nil => optionMap
        case "--master" :: value :: tail           => nextOption(optionMap ++ Map("master" -> value.toString), tail)
        case "--s3-access-key" :: value :: tail    => nextOption(optionMap ++ Map("s3AccessKey" -> value.toString), tail)
        case "--s3-secret-key" :: value :: tail    => nextOption(optionMap ++ Map("s3SecretKey" -> value.toString), tail)
        case "--s3-bucket-name" :: value :: tail   => nextOption(optionMap ++ Map("s3BucketName" -> value.toString), tail)
        case "--test-duration" :: value :: tail        => nextOption(optionMap ++ Map("testDuration" -> value.toString), tail)
        case "--write-to" :: value :: tail        => nextOption(optionMap ++ Map("writeTo" -> value.toString), tail)
        case string :: Nil => optionMap
      }
    }
    nextOption(Map(), argList)
  }

  def setLogging(level: Level): Unit = {
    Logger.getLogger("org.apache.spark"                     ).setLevel(level)
    Logger.getLogger("org.spark_project"                    ).setLevel(level)
    Logger.getLogger("org.apache.hadoop"                    ).setLevel(level)
    Logger.getLogger("akka"                                 ).setLevel(level)
    Logger.getLogger("com.datastax.spark"                   ).setLevel(level)
    Logger.getLogger("com.datastax.driver"                  ).setLevel(level)
    Logger.getLogger("com.amazonaws.services.kinesis"       ).setLevel(level)
  }

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")
    result
  }

  def listFiles(path: String):  Array[String] = {
    val fs = FileSystem.get(new java.net.URI(path), new Configuration())
    fs.listStatus(new Path(path)).flatMap(s => Seq(s.getPath.toString))
  }

  override def main(args: Array[String]): Unit = {
    // Parse arguments
    val argList = args.toList
    val options = listToOptionMap(argList)
    val master             = options("master").toString
    val s3AccessKey        = options.getOrElse("s3AccessKey","").toString
    val s3SecretKey        = options.getOrElse("s3SecretKey", "").toString
    val s3BucketName       = options.getOrElse("s3BucketName", "").toString
    val testDuration       = options.getOrElse("testDuration", "minute").toString
    val writeTo            = options.getOrElse("writeTo", "").toString
    //    log.info(options)

    // Set logging
    setLogging(Level.INFO)

    // CREATE SPARK SESSION
    val spark = SparkSession
      .builder()
      .appName(getClass.getName)
      .master(master)
      .getOrCreate()
    val sc = spark.sparkContext
    val schema = SchemaConverter.convert("/proxyLogsSchema.json")

    if (options("master").toString  == "local[*]") {
      spark.conf.set("spark.driver.bindAddress", "127.0.0.1")
    }
    SparkEnv.get.conf.set("spark.debug.maxToStringFields","8192")
    sc.hadoopConfiguration.set("mapreduce.fileoutputcommitter.algorithm.version", "2")
    sc.hadoopConfiguration.set("fs.s3a.fast.upload","true")
    val pageLength = 10000
    def s3 = new AmazonS3Client(new BasicAWSCredentials(s3AccessKey,s3SecretKey))

    def testDurationFunc(testOpt: String): Seq[String] = {
      if (testOpt == "week") {
        val days = for (d <- 1 to 7) yield s"%02d".format(d)
        val hours = for (h <- 0 to 23) yield s"%02d".format(h)
        val minutes = for (m <- 0 to 59) yield s"%02d".format(m)
        val paths = for {day <- days; hour <- hours; minute <- minutes} yield s3prefix(master)+s"/raw_access_log/day=$day/hour=$hour/minute=$minute/"
        paths
      } else if (testOpt == "day") {
        val hours = for (h <- 0 to 23) yield s"%02d".format(h)
        val minutes = for (m <- 0 to 59) yield s"%02d".format(m)
        val paths = for {hour <- hours; minute <- minutes} yield s3prefix(master)+s"/raw_access_log/day=01/hour=$hour/minute=$minute/"
        paths
      } else if (testOpt == "hour") {
        val minutes = for (m <- 0 to 59) yield s"%02d".format(m)
        val paths = for {minute <- minutes} yield s3prefix(master)+s"/raw_access_log/day=01/hour=00/minute=$minute/"
        paths
      } else if (testOpt == "minute") {
        IndexedSeq(s3prefix(master)+"/raw_access_log/day=01/hour=00/minute=00/")
      } else {
        IndexedSeq(s3prefix(master)+"/raw_access_log/day=01/hour=00/minute=00/")
      }
    }
    def combinedDF(s3Paths: String): (DataFrame) = {
      var outputDF = spark.createDataFrame(sc.emptyRDD[Row], schema)
      s3Paths.split(",").foreach { s3Path =>
        val regex = """(?i)^s3a://([a-zA-Z]{7,}\:.{7,}\@)([^/]+)/(.*)""".r
        //        val regex = """(?i)^s3a://([^/]+)/(.*)""".r
        val bucket = regex.findFirstMatchIn(s3Path).map(_ group 2).getOrElse(null)
        val prefix = regex.findFirstMatchIn(s3Path).map(_ group 3).getOrElse(null)
        println("Processing s3 resource: bucket '%s', prefix '%s'".format(bucket, prefix))
        @transient val request = new ListObjectsRequest()
        request.setBucketName(bucket)
        request.setPrefix(prefix)
        request.setMaxKeys(pageLength)
        @transient var listing = s3.listObjects(request)
        var proceed = true
        while (proceed) {
          if (listing.getObjectSummaries.isEmpty) {
            proceed = false
          } else {
            @transient val s3FileKeys = listing.getObjectSummaries.asScala.map(_.getKey).toList
            val inputLines = sc.parallelize(s3FileKeys).flatMap { key =>
              Source.fromInputStream(new GZIPInputStream(new BufferedInputStream(s3.getObject(bucket, key).getObjectContent: InputStream))).getLines()}
            val miniDF = spark.sqlContext.read.schema(schema).json(inputLines)
            outputDF = outputDF.union(miniDF)
            listing = s3.listNextBatchOfObjects(listing)
          }
        }
      }
      outputDF
    }

    def writeToFunc(writeOpt: String): String = {
      if (writeOpt == "s3") {
        s3prefix(master)+"/parquet_tests/test.parquet"
      } else if (writeOpt == "hdfs") {
        "hdfs:///test.parquet"
      } else{
        ""
      }
    }

    def s3prefix(masterOpt: String): String = {
      if (masterOpt  == "local[*]") {
        s"s3a://$s3AccessKey:$s3SecretKey@$s3BucketName"
      }
      else {
        s"s3a://$s3BucketName"
      }
    }
    val pathsRDD = sc.parallelize(testDurationFunc(testDuration))
    val filesRDD = pathsRDD.flatMap(p => listFiles(p))
    val s3PathsTest = filesRDD.reduce((s1, s2) => s1 + "," + s2)
    val df = combinedDF(s3PathsTest)
    if (writeToFunc(writeTo) != "") {
      df.write.mode("overwrite").format("parquet").save(writeToFunc(writeTo))
    }
    spark.stop()
  }
}