package com.wandera.datascience.spark.sink

import java.io._
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.PutObjectRequest
import com.wandera.datascience.spark.sink.S3Sink.log
import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.Saveable
import org.apache.spark.rdd.RDD

trait S3Sink {

  Logger.getLogger("org.apache.parquet").setLevel(Level.ERROR)

  private val S3AccessKey: String = ""
  private val S3SecretKey: String = ""
  private val Bucket: String = ""
  private val Prefix = ""

  private val tmpDirectory = "tmp"
  private val bufferSize = 8192

  def sinkPath(sinkDirectory: String): String = {
    val path = s"s3a://$S3AccessKey:$S3SecretKey@$Bucket/$Prefix$sinkDirectory"
    log.debug(s"S3 Sink Path = $path")
    path
  }

  def saveData(data: RDD[LabeledPoint], sinkDirectory: String) = {
    val path = sinkPath(sinkDirectory) + "/data"
    log.info(s"About to save data to S3 under path '$path'.")
    data.saveAsObjectFile(path)
  }

  def saveModel(model: Saveable,
                sinkDirectory: String,
                sparkContext: SparkContext) = {
    val path = sinkPath(sinkDirectory) + "/model"
    log.info(s"About to save model to S3 under path '$path'.")
    model.save(sparkContext, path)
  }

  def saveOneVsRestModels(models: IndexedSeq[Saveable],
                          sinkDirectory: String,
                          sparkContext: SparkContext) = {
    val path = sinkPath(sinkDirectory) + "/models"
    log.info(s"About to save model to S3 under path '$path'.")
    models.zipWithIndex.foreach(mi => {
      val model = mi._1
      val index = mi._2
      model.save(sparkContext, path + s"/$index")
    })
  }

  private def createTempDir() = {
    val tmpFile = new File(tmpDirectory)
    if (!tmpFile.exists()) tmpFile.mkdir()
  }

  def uploadFileToS3(file: File,
                     fileName: String,
                     sinkDirectory: String,
                     accessKey: String = S3AccessKey,
                     secretKey: String = S3SecretKey,
                     bucketName: String = Bucket) = {
    val s3client = new AmazonS3Client(
      new BasicAWSCredentials(accessKey, secretKey))

    val key = s"$Prefix$sinkDirectory/$fileName"
    val absoluteFileName = file.getAbsoluteFile

    log.info(
      s"About to upload file '$absoluteFileName' to S3 under bucket '$bucketName' and key '$key'.")
    s3client.putObject(new PutObjectRequest(bucketName, key, file))
    log.info(
      s"Finished uploading file '$absoluteFileName' to S3 under bucket '$bucketName' and key '$key'.")
  }

}

object S3Sink {
  @transient lazy val log = LogManager.getLogger(getClass.getName)
}